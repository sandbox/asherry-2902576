<?php

function content_importer_fetch_form($form_state) {
  // Is needed to load the different backends as "modules"
  content_importer_prepare_backends();

  $form['step_1'] = array(
    '#type' => 'fieldset',
    '#title' => t('Step 1'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['step_2'] = array(
    '#type' => 'fieldset',
    '#title' => t('Step 2'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // For step one show the different CMS that we support.
  $backends = module_invoke_all('provides');
  asort($backends);
  $form['step_1']['plugin'] = array(
    '#type' => 'radios',
    '#title' => t('What is the system that you are migratring from?'),
    '#options' => $backends,
    '#required' => TRUE,
    '#description' => t('PUT HELPFUL INFO HERE.'),
  );

  // For step two, get the file to retrieve.
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  $form['step_2']['library'] = array(
    '#type' => 'file',
    '#title' => t('Import File'),
    '#description' => t('PUT HELPFUL INFO HERE.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import Content'),
  );
  return $form;
}

function content_importer_fetch_form_submit($form_id, $form_values) {
  // Save the file.
  $file = file_save_upload('library');
  if (!$file) {
    form_set_error('library', t('No valid file was found.'));
    return;
  }

  if (module_hook($form_values['values']['plugin'], 'parse_fields')) {
    $fields = module_invoke($form_values['values']['plugin'], 'parse_fields');
    content_importer_xml_parser($form_values['values']['plugin'], $file->filepath);
  }
  else {
    module_invoke($form_values['values']['plugin'], 'parse', $file->filepath);
  }
}